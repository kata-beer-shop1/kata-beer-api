import {
    Body,
    ClassSerializerInterceptor,
    ConflictException,
    Controller,
    Get,
    NotFoundException,
    Param,
    Post,
    UnprocessableEntityException,
    UseInterceptors
} from '@nestjs/common'
import { CreateUserDto } from '../dto/create-user.dto'
import { UserService } from '../service/user.service'
import { Auth } from '../../auth/decorator/auth.decorator'
import {
    ApiUnprocessableEntityResponse,
    ApiCreatedResponse,
    ApiTags,
    ApiOkResponse,
    ApiNotFoundResponse,
    ApiOperation
} from '@nestjs/swagger'
import { User } from '../entity/user.entity'
import { ExceptionDto } from '../../middleware/exception/dto/exception.dto'
import { ExceptionMessages } from '../../middleware/exception/constants/constants'

@ApiTags('Users')
@Controller('users')
@UseInterceptors(ClassSerializerInterceptor)
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Post()
    @ApiOperation({ summary: 'Create a user' })
    @ApiCreatedResponse({ description: 'User successfully created', type: User })
    @ApiUnprocessableEntityResponse({
        description: ExceptionMessages.UNPROCESSABLE_EXCEPTION,
        type: ExceptionDto
    })
    async create(@Body() createUserDto: CreateUserDto) {
        const existingUser = await this.userService.findByEmail(createUserDto.email)
        if (existingUser) {
            throw new ConflictException(`User ${createUserDto.email} already exists`)
        }
        createUserDto.password = await this.userService.hashPassword(createUserDto.password)
        try {
            const user = await this.userService.create(createUserDto)
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const { password, ...result } = user
            return result
        } catch (e) {
            throw new UnprocessableEntityException(e)
        }
    }

    @Get(':email')
    @Auth()
    @ApiOperation({ summary: 'Find a user by email' })
    @ApiOkResponse({ description: 'User successfully retrieved', type: User })
    @ApiNotFoundResponse({
        description: ExceptionMessages.NOT_FOUND_EXCEPTION,
        type: ExceptionDto
    })
    async findByEmail(@Param('email') email: string) {
        try {
            return await this.userService.findByEmail(email)
        } catch (e) {
            throw new NotFoundException()
        }
    }
}
