import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'
import { IsEmail, IsNotEmpty, IsNumber, IsString } from 'class-validator'
import { Exclude } from 'class-transformer'
import { Role } from '../enum/user.enum'
import { ApiProperty } from '@nestjs/swagger'

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    @IsNumber()
    @ApiProperty()
    id: number

    @IsEmail()
    @Column()
    @IsNotEmpty()
    @ApiProperty({ description: 'User email' })
    email: string

    @Exclude()
    @IsString()
    @Column()
    @IsNotEmpty()
    @ApiProperty({ description: 'User password' })
    password: string

    @IsString()
    @Column({ default: Role.Customer })
    @ApiProperty({ enum: Role, default: Role.Customer })
    role: Role
}
