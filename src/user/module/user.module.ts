import { Module } from '@nestjs/common'
import { UserService } from '../service/user.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from '../entity/user.entity'
import { UserController } from '../controller/user.controller'
import { CaslModule } from '../../casl/module/casl.module'

@Module({
    imports: [TypeOrmModule.forFeature([User]), CaslModule],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService]
})
export class UserModule {}
