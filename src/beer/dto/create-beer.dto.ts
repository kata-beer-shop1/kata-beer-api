import { Beer } from '../entity/beer.entity'
import { OmitType } from '@nestjs/swagger'

export class CreateBeerUserDto extends OmitType(Beer, ['id', 'owner_id']) {}
export class CreateBeerDto extends OmitType(CreateBeerUserDto, ['owner']) {}
