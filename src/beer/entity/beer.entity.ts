import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, RelationId } from 'typeorm'
import { IsNotEmpty, IsNumber, IsString } from 'class-validator'
import { Exclude } from 'class-transformer'
import { ApiProperty } from '@nestjs/swagger'
import { User } from '../../user/entity/user.entity'

@Entity()
export class Beer {
    @PrimaryGeneratedColumn()
    @IsNumber()
    @IsNotEmpty()
    @ApiProperty({ description: 'Beer technical id' })
    id: number

    @IsString()
    @Column()
    @IsNotEmpty()
    @ApiProperty({ description: 'An image that represents the beer' })
    image: string

    @IsString()
    @Column()
    @IsNotEmpty()
    @ApiProperty({ description: 'Beer name' })
    name: string

    @IsString()
    @Column()
    @IsNotEmpty()
    @ApiProperty({ description: 'A description of its taste' })
    description: string

    @IsNumber()
    @Column()
    @IsNotEmpty()
    @ApiProperty({ description: 'The quantity in cl in a bottle' })
    volume: number

    @IsString()
    @Column()
    @IsNotEmpty()
    @ApiProperty({ description: 'The ingredients used to brew it' })
    ingredients: string

    @IsString()
    @Column()
    @IsNotEmpty()
    @ApiProperty({ description: 'Tips to make it perfect' })
    brewers_tips: string

    @ManyToOne(() => User, { onDelete: 'CASCADE', eager: true })
    owner: User

    @Exclude()
    @RelationId((beer: Beer) => beer.owner)
    owner_id: number
}
