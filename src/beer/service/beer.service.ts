import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Beer } from '../entity/beer.entity'
import { CreateBeerUserDto } from '../dto/create-beer.dto'
import { UpdateBeerDto } from '../dto/update-beer.dto'

@Injectable()
export class BeerService {
    constructor(
        @InjectRepository(Beer)
        private beerRepository: Repository<Beer>
    ) {}

    async findAll(): Promise<Beer[]> {
        return this.beerRepository.find()
    }

    async findById(id: number): Promise<Beer | null> {
        return this.beerRepository.findOneBy({ id })
    }

    async findByName(name: string): Promise<Beer | null> {
        return this.beerRepository.findOneBy({ name })
    }

    async create(beer: CreateBeerUserDto) {
        return this.beerRepository.save(beer)
    }

    async update(id: number, beer: UpdateBeerDto) {
        return this.beerRepository.update(id, beer)
    }
}
