import { User } from '../../user/entity/user.entity'

export type UserRequestDto = Request & { user: User }
