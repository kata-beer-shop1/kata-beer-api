import { Module } from '@nestjs/common'
import { AuthService } from '../service/auth.service'
import { PassportModule } from '@nestjs/passport'
import { LocalStrategy } from '../strategy/local.strategy'
import { JwtStrategy } from '../strategy/jwt.strategy'
import { JwtModule } from '@nestjs/jwt'
import { ConfigService } from '@nestjs/config'
import { UserModule } from '../../user/module/user.module'
import { AuthController } from '../controller/auth.controller'

@Module({
    imports: [
        PassportModule,
        UserModule,
        JwtModule.registerAsync({
            useFactory: (configService: ConfigService) => {
                return {
                    secret: configService.get('JWT_SECRET_KEY'),
                    signOptions: {
                        expiresIn: configService.get('JWT_TOKEN_EXPIRATION')
                    }
                }
            },
            inject: [ConfigService]
        })
    ],
    providers: [AuthService, JwtStrategy, LocalStrategy],
    controllers: [AuthController]
})
export class AuthModule {}
