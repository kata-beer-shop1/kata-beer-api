import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common'
import { User } from '../../user/entity/user.entity'
import { UserService } from '../../user/service/user.service'
import { JwtService } from '@nestjs/jwt'
import { SignInDto } from '../dto/SignIn.dto'
import { JwtTokenDto } from '../dto/JwtToken.dto'

@Injectable()
export class AuthService {
    constructor(private userService: UserService, private jwtService: JwtService) {}

    async validateUser(email: string, password: string): Promise<User> {
        const user = await this.userService.findByEmail(email)
        if (!user) {
            throw new NotFoundException(`User with email ${email} does not exist`)
        }
        if (await this.userService.verifyPassword(user.password, password)) {
            return user
        }
        throw new UnauthorizedException('Passwords do not match')
    }

    async signIn(signInDto: SignInDto): Promise<JwtTokenDto> {
        const user = await this.userService.findByEmail(signInDto.email)
        const payload = {
            email: user.email,
            sub: user.id
        }

        return {
            access_token: this.jwtService.sign(payload)
        }
    }
}
