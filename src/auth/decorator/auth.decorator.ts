import { applyDecorators, UseGuards } from '@nestjs/common'
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger'
import { JwtAuthGuard } from '../guard/jwt-auth.guard'
import { ExceptionMessages } from '../../middleware/exception/constants/constants'
import { ExceptionDto } from '../../middleware/exception/dto/exception.dto'

export function Auth() {
    return applyDecorators(
        UseGuards(JwtAuthGuard),
        ApiBearerAuth(),
        ApiUnauthorizedResponse({
            description: ExceptionMessages.UNAUTHORIZED_EXCEPTION,
            type: ExceptionDto
        })
    )
}
