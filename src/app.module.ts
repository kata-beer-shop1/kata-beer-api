import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import { BeerModule } from './beer/module/beer.module'
import { UserModule } from './user/module/user.module'
import { AuthModule } from './auth/module/auth.module'
import { CaslModule } from './casl/module/casl.module'
import { load } from './config/config'
import databaseOptions from './config/database'

@Module({
    imports: [
        ConfigModule.forRoot(load()),
        TypeOrmModule.forRootAsync(databaseOptions()),
        BeerModule,
        UserModule,
        AuthModule,
        CaslModule
    ]
})
export class AppModule {}
