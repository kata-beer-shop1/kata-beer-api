export const corsOptions = () => {
    return {
        origin: 'http://localhost:3000',
        methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
        allowedHeaders: ['Accept', 'Content-Type', 'Authorization'],
        maxAge: 86400
    }
}
