# Beer Rest API

## Subject

An en E-commerce API that allows customers to purchase beers and administrators to manage the store.  
The API should be developed using NestJS, TypeScript and PostgreSQL

There is little to no business case in this project. It focuses on using the latest and best practices of NestJS and TypeScript.  
Your project should be structured around the 3 main NestJS components (Controller, Provider, Module).

You should implement (at least) the following routes :    
- Retrieve all beers
- Create a user  
- Log a user in
- Update a beer

+ Any other route you wish to implement 


## Requirements

### Dockerization

Your project should run inside a docker container, as well as your database.
You should document how to run the container inside a README.md

### Authentication and authorization

Restrict one or multiple routes to authenticated users.  
Check the authenticated user it allowed to perform a certain action on a specific resource (ie: A user should not be 
allowed to update a beer created by another user). 

### E2E Tests

Implement E2E tests using Jest (unit tests aren't required as there is no business logic) and run them on a CI/CD using .gitalb-ci.yml file

### Documentation

Make sure you provide a documentation using Swagger, an url to access it and a JSON file to query the API directlry from the repository

## Additional advices

### Input validation

Make sure you validate every input at runtime before any processing. Make sure static and runtime type are in sync.

### Secret management

Use a hash for any sensitive data you should not store as plain text.  
Use environment variables as provided in the .env.example file.  

### Package management

Properly use dependencies and devDependencies in your package.json

### Code quality

Use at least a linter dependency (you can add a code prettier)




