import { faker } from '@faker-js/faker'
import { Role } from '../../src/user/enum/user.enum'

export const getFakeUser = (userId: number) => {
    return {
        id: userId,
        email: faker.internet.email(),
        password: faker.internet.password(),
        role: Role.Customer
    }
}
