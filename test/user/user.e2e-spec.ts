import * as request from 'supertest'
import { UserModule } from '../../src/user/module/user.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { INestApplication } from '@nestjs/common'
import { User } from '../../src/user/entity/user.entity'
import { Repository } from 'typeorm'
import { UserService } from '../../src/user/service/user.service'
import { Test } from '@nestjs/testing'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { instanceToPlain } from 'class-transformer'
import { clearDatabase } from '../spec-helper'
import { AuthService } from '../../src/auth/service/auth.service'
import { JwtService } from '@nestjs/jwt'
import { AuthModule } from '../../src/auth/module/auth.module'
import { getFakeUser } from '../fake/user.fake'
import databaseOptions from '../../src/config/database'
import { load } from '../../src/config/config'
import { Role } from '../../src/user/enum/user.enum'

describe('UserController (e2e)', () => {
    let authService: AuthService
    let userService: UserService

    let configService: ConfigService

    let userRepository: Repository<User>

    let app: INestApplication

    beforeAll(async () => {
        const moduleFixture = await Test.createTestingModule({
            imports: [
                UserModule,
                AuthModule,
                ConfigModule.forRoot(load()),
                TypeOrmModule.forRootAsync(databaseOptions())
            ]
        }).compile()

        app = moduleFixture.createNestApplication()
        await app.init()

        userRepository = moduleFixture.get('UserRepository')
        userService = new UserService(userRepository)

        configService = new ConfigService()

        authService = new AuthService(
            userService,
            new JwtService({
                secret: configService.get<string>('JWT_SECRET_KEY'),
                signOptions: { expiresIn: configService.get<string>('JWT_TOKEN_EXPIRATION') }
            })
        )
    })

    beforeEach(async () => {
        await clearDatabase(app)
    })

    describe('Create an account for a user', () => {
        it(`/POST users`, async () => {
            const user = getFakeUser(1)

            await request(app.getHttpServer())
                .post('/users')
                .send(user)
                .expect(201)
                .expect(async () => {
                    return JSON.stringify(instanceToPlain(await userService.findAll()))
                })
        })
    })

    describe('Retrieve a user account by email', () => {
        it(`/GET users/email`, async () => {
            const user = await userService.create(getFakeUser(1))
            const jwt = await authService.signIn(user)

            await request(app.getHttpServer())
                .get('/users/' + user.email)
                .set('Authorization', 'Bearer ' + jwt.access_token)
                .expect(200)
                .expect(
                    JSON.stringify(
                        instanceToPlain({
                            id: user.id,
                            email: user.email,
                            role: Role.Customer
                        })
                    )
                )
        })
    })

    afterAll(async () => {
        await app.close()
    })
})
